CREATE TABLE IF NOT EXISTS playbook_results (
 id INTEGER PRIMARY KEY,
 project_title text NOT_NULL,
 job_title text NOT_NULL,
 inventory_file text NOT_NULL,
 limittag text NOT_NULL,
 total_host_count INTEGER NOT_NULL,
 unreachable_host_count INTEGER NOT_NULL,
 failed_host_count INTEGER NOT_NULL,
 failed_facts_host_count INTEGER NOT_NULL,
 successful_host_count INTEGER NOT_NULL,
 no_ssh_avail_count INTEGER NOT_NULL,
 no_dns_found_count INTEGER NOT_NULL,
 total_host_list text NOT_NULL,
 successful_host_list text NOT_NULL,
 failed_host_list text NOT_NULL,
 failed_facts_host_list text NOT_NULL,
 unreachable_host_list text NOT_NULL, 
 no_ssh_avail_list text NOT_NULL,
 no_dns_found_list text NOT_NULL,
 finish_date text NOT_NULL,
 finish_time text NOT_NULL
);
  
